CREATE DATABASE decisions_test;
CREATE USER decisions_test WITH ENCRYPTED PASSWORD 'NeverInProduction5678';
GRANT ALL PRIVILEGES ON DATABASE decisions_test TO decisions_test;

\connect decisions_test;
-- CREATE SCHEMA decisions_test AUTHORIZATION decisions_test; -- Decisions does not support alternate schemas
GRANT CREATE ON SCHEMA public TO decisions_test; 

