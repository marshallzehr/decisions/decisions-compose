# decisions-compose

Starts a Decisions application server with backing Postgres database using Docker Compose.  This server has no license,
so it is of limited utility other than for diagnosing startup issues and/or serving as a starting place for more
comprehensive configurations.

Originally used to reproduce the startup issues we had with 8.12.0.71930, the necessary configurations for which are
detailed in the Configuration Notes section below.

## Contents

* Decisions 8.18.0.72888
* PostgreSQL 15.5
* Adminer (for exploring the Postgres database)

## Running

To run interactively, exiting if any of the containers exit:

```
docker compose up --abort-on-container-exit
```

To run in the background:

```
docker compose up -d
```

To get rid of the environment entirely:

```
docker compose down --volumes
```

## Usage

Default ports are used inside the Docker network. Exposed outside the network:

* **Decisions on port 8000**: http://localhost:8000
* **Postgres on port 8001**: `psql --host localhost --port 8001 --username decisions_test --dbname decisions_test`
* **Adminer on port 8002**: http://localhost:8002

The default login (from [here](https://documentation.decisions.com/docs/installation-guide)) is:

* **Username**: `admin@decisions.com`
* **Password**: `admin`

You can connect to the database using the created Decisions database user:

* **System**: `PostgreSQL`
* **Server**: `postgres`
* **Username**: `decisions_test`
* **Password**: `NeverInProduction5678`
* **Database**: `decisions_test`

You can browse the file volume with:

```
docker compose exec decisions bash
```

Finally, if you want to run a Repository rather than an Application server, set the server type variable by overriding
with `docker-compose.override.yml`.  You can clone this from the sample provided:

```
cp docker-compose.override.repository.yml docker-compose.override.yml
```

## Configuration Notes

This repo contains workarounds for the following issues:

* PostgreSQL 15 [removed PUBLIC creation permission on the public schema](https://www.postgresql.org/docs/release/15.0/),
  per their recommendations going back to 2018
  * *SYMPTOM*: `Error message: 42P01: relation "ObjectStructureCheck" does not exist`
  * Decisions doesn't log table create errors, so the error you get is on first access
* Postgres [recommends using user-private schemas](https://www.postgresql.org/docs/15/ddl-schemas.html#DDL-SCHEMAS-PATTERNS)
  * *SYMPTOM*: `Error message: 42P07: relation "invitation" already exists`.
  * Decisions doesn't like schemas that are not `public`, but only sometimes.
* `DECISIONS_FILESTORAGELOCATION` should match the data mount path of `/opt/decisions/data` (per support) - not what
  [the documentation](https://documentation.decisions.com/docs/creating-environment-list-files-env-list) specifies in
  the example
  * *SYMPTOM*: Data volume is empty

With these workarounds, the Decisions application/repository servers start as expected.